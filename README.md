# sblog

#### 介绍
我的个人博客，用于搭建知识仓库，访问地址[司某博客](http://alinspore.gitee.io/sblog/)

> VuePress 由两部分组成：第一部分是一个极简静态网站生成器，它包含由 Vue 驱动的主题系统和插件 API，另一个部分是为书写技术文档而优化的默认主题，它的诞生初衷是为了支持 Vue 及其子项目的文档需求。
>
> 每一个由 VuePress 生成的页面都带有预渲染好的 HTML，也因此具有非常好的加载性能和搜索引擎优化（SEO）。同时，一旦页面被加载，Vue 将接管这些静态内容，并将其转换成一个完整的单页应用（SPA），其他的页面则会只在用户浏览到的时候才按需加载。

#### 软件架构
vuepress+gitee部署

| 序号 | 软件    | 版本       | 备注            |
| ---- | ------- | ---------- | --------------- |
| 1    | node.js | v16.19.0   |                 |
| 2    | npm     | V8.19.3    |                 |
| 3    | gitee   | 个人免费版 | 需开通giteepage |


#### 安装教程

### 本地博客搭建命令

1. npm 博客搭建

```bash
npm install @vuepress-reco/theme-cli -g #插件安装
theme-cli init wangzg-blog #项目初始化
cd wangzg-blog 
npm install #安装依赖
npm run dev #项目运行
npm run build #项目构建
```

2. yarn 博客搭建(使用一种即可)

```
npm  install -g cnpm --registry=https://registry.npm.taobao.org #安装淘宝镜像
cnpm install -g yarn #安装yarn
yarn global add @vuepress-reco/theme-cli #插件安装
theme-cli init wangzg-blog # init #项目初始化
cd wangzg-blog 
yarn install #安装依赖
yarn dev #项目运行
yarn build #项目构建
```

3. 项目运行效果

   ![image-20230821102947153](public/img/image-20230821102947153.png)

   4. 参考链接

   - vuepress 快速上手：[https://vuepress.vuejs.org/zh/guide/getting-started.html](https://link.zhihu.com/?target=https%3A//vuepress.vuejs.org/zh/guide/getting-started.html)

   - vuepress-theme ：[https://github.com/topics/vuepress-theme](https://link.zhihu.com/?target=https%3A//github.com/topics/vuepress-theme)

   - awesome-vuepress ：[https://github.com/meteorlxy/awesome-vuepress](https://link.zhihu.com/?target=https%3A//github.com/meteorlxy/awesome-vuepress)

   - vuepress博客主题—vuepress-theme-reco ： [https://juejin.im/post/5c80de10f265da2dcd7a0e5e](https://link.zhihu.com/?target=https%3A//juejin.im/post/5c80de10f265da2dcd7a0e5e)

   - vuepress-theme-blog ： [https://vuepress-theme-blog.ulivz.com/](https://link.zhihu.com/?target=https%3A//vuepress-theme-blog.ulivz.com/)

   - VuePress 快速踩坑 ： [https://juejin.im/entry/5ae178e](https://link.zhihu.com/?target=https%3A//juejin.im/entry/5ae178ebf265da0b84552035)

#### 使用说明

1.  浏览器访问：[司某博客](http://alinspore.gitee.io/sblog/)

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
