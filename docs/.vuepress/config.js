
module.exports = { 
    
    title: '司某的博客',
    description: 'Just playing around',
    base: '/sblog/',
    head: [
        ['link', { rel: 'icon', href: '/img/logo.png' }]
    ],
    theme: 'reco',
    markdown: {
        lineNumbers: true
    },
    "sidebar": {
        "/docs/theme-reco/": [
            "",    // 空字符串对应的是 README.md
            "theme",  // 对应 theme.md
            "plugin",  // 对应plugin.md
            "api"    // 对应api.md
        ]
    },

    // 插件模块
    plugins: {
        // 
        // 页面滚动时自动激活侧边栏链接的插件
        // 
        "@vuepress/active-header-links": {
            sidebarLinkSelector: ".sidebar-link",
            headerAnchorSelector: ".header-anchor",
        },

        // 看板娘
      /*  '@vuepress-reco/vuepress-plugin-kan-ban-niang':
        {
            theme: ['blackCat', 'whiteCat', 'haru1', 'haru2', 'haruto', 'koharu', 'izumi', 'shizuku', 'wanko', 'miku', 'z16']
        },*/
        "@vuepress-reco/vuepress-plugin-kan-ban-niang":
        {
            theme: ['blackCat', 'whiteCat', 'haru1', 'haru2', 'haruto', 'koharu', 'izumi', 'shizuku', 'wanko', 'miku', 'z16'],
            clean: false,
            messages: {
                welcome: '我是lookroot欢迎你的关注 ',
                home: '心里的花，我想要带你回家。',
                theme: '好吧，希望你能喜欢我的其他小伙伴。',
                close: '再见哦'
            },
            width: 240,
            height: 352
        },
        "ribbon":
        {
          size: 90,     // width of the ribbon, default: 90
          opacity: 0.3, // opacity of the ribbon, default: 0.3
          zIndex: -1    // z-index property of the background, default: -1
        },
    /*    "cursor-effects":
      {
        size: 3,                    // size of the particle, default: 2
        shape: ['circle'],  // shape of the particle, default: 'star'
        zIndex: 999999999           // z-index property of the canvas, default: 999999999
      },
      'flowchart': '',
      'sitemap': {
        hostname: 'https://www.glassysky.site'
      },*/

        // 公告栏弹窗

        "@vuepress-reco/vuepress-plugin-bulletin-popover":
        {
            width: "300px", // 默认 260px
            title: "消息提示",
            body: [
                {
                    type: "title",
                    content: "欢迎加入QQ交流群 🎉🎉🎉",
                    style: "text-aligin: center;",
                },
                {
                    type: "image",
                    src: "/logo.png",
                    style: "width:100px;height:100px;border-radius:50%;",
                },
            ],
            footer: [
                {
                    type: "button",
                    text: "打赏",
                    link: "/",
                },
            ]
        },
        // 添加著作权信息

        'copyright':
        {
            authorName: 'onion', // 选中的文字将无法被复制
            minLength: 30, // 如果长度超过  30 个字符
        },


    },
    themeConfig: {
        type: 'blog',
        subSidebar: 'auto',//在所有页面中启用自动生成子侧边栏，原 sidebar 仍然兼容

        //导航
        nav: [
            { text: "首页", link: "/", icon: 'reco-home' },
            { text: 'TimeLine', link: '/timeline/', icon: 'reco-date' },
            {
                "text": "数据库",
                "icon": "reco-message",
                "items": [
                    {
                        "text": "MySQL",
                        "link": "/docs/theme-reco/"
                    },
                    {
                        "text": "Postgresql",
                        "link": "/docs/theme-reco/"
                    },
                ]
            },
        ],
        // 博客配置
        blogConfig: {
            category: {
                location: 3, // 在导航栏菜单中所占的位置，默认2
                text: "博客", // 默认文案 “分类”
            },
            tag: {
                location: 4, // 在导航栏菜单中所占的位置，默认3
                text: "Tag", // 默认文案 “标签”
            },

        },
        logo: '/img/logo.png',
        authorAvatar: '/img/logo.png',
        friendLink: [
            {
                title: "vuepress-theme-reco",
                desc: "A simple and beautiful vuepress Blog & Doc theme.",
                logo: "https://vuepress-theme-reco.recoluan.com/icon_vuepress_reco.png",
                link: "https://vuepress-theme-reco.recoluan.com",
            }
            // ...
        ]
    },
    // .vuepress/config.js
    // 备案号
    record: 'xxxx',
    // 项目开始时间
    startYear: '2017'



}
