---
title: 【Linux】 Linux备忘手册
date: 2023-08-15
categories:
  - 服务器
tags:
  - Linux
author: 司雄斌
---

::: tip
Linux 备忘手册
:::

![图片](/img/01linux.jpg)
![图片](/img/02页-文件与目录管理.jpg)
![图片](/img/03页-文件与目录管理2.jpg)
![图片](/img/04页-Wim文本编辑和文件管理.jpg)
![图片](/img/05页-系统管理篇.jpg)
![图片](/img/06页-认识BASH和基本命令.jpg)
![图片](/img/07页-Shell基础命令和文本操作.jpg)
![图片](/img/08页-文本操作相关命令.jpg)