---
title: 【笔记】 Markdown语法整理
date: 2023-08-15
categories:
  - 服务器
tags:
  - md
author: 司雄斌
---

::: tip
Markdown 语法整理
:::

标题：

# 一级标题
## 二级标题
### 三级标题
#### 四级标题
##### 五级标题
###### 六级标题

注： vuepress-theme-reco主题中正文是从二级标题开始的，二级标题及以下会生成相应的侧边栏

【文字】

加粗

**内容**

斜体

*内容*

删除线

~~内容~~

【列表】

有序列表
1. java
2. C#
3. bash

无序列表

- java
- C#
- shell

【引用】

> 内容

【代码块】

```c
#include<stdio.h>
```

```sql
select * from student;
select * from student;

```

【图片】
![图片](/img/logo.png)

【表格】
| 列标题1 | 列标题2 | 列标题3|
|--|--|--|
| 内容 | 内容 | 内容 |

【超链接】
[百度](www.baidu.com)